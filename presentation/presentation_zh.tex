% Template: Fabian Wenzelmann, 2019
\documentclass{beamer}

\usepackage[utf8]{inputenc}
%\usepackage[english]{babel} % use ngerman here for german text
\usepackage[T1]{fontenc}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{algorithm,algpseudocode}
\usepackage{lmodern}
\usepackage[protrusion=true,expansion=true,kerning]{microtype}
\usepackage{graphicx}
\usepackage{hyperref}

\usetheme{CambridgeUS}
\usecolortheme{dolphin}

\title[MST LogP]{Distributed Graph Algorithms in the LogP Model}
% \subtitle{Subtitle}

% For short you could for example use the last name only, it's optional as is
% the short title
\author[Häringer]{Zacharias Häringer}

% Can be set, for students usually not required
%\institute{}

\date{\today}

% removes the navigation symbols
% often they don't work and I find them rather annoying
\beamertemplatenavigationsymbolsempty{}

% You can have a logo to appear on all slides
% \logo{\includegraphics[height=1.5cm]{logo}}

% If you want that at the beginning of each section the table of contents
% is shown, I don't like it for short presentations
%\AtBeginSection[]
%{
%  \begin{frame}
%    \frametitle{Table of Contents}
%    \tableofcontents[currentsection]
%  \end{frame}
%}

\newcommand{\boruvka}{Bor\r uvka }
\newcommand{\boruvkas}{Bor\r uvka's }

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

% TOC if you want it
\begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents
\end{frame}

\section{The MST Problem}
\begin{frame}
	\frametitle{The MST Problem}
    
    \begin{itemize}
    	\item<1->
    	\begin{minipage}{.45\textwidth}
    		Network: undirected simple Graph \\
    		$G=(V, E, w)$
    	\end{minipage}
    	\begin{minipage}{.45\textwidth}
    		\begin{figure}
    			\includegraphics[width=\textwidth]{figures/simpleGraphWithWeights.png}
    		\end{figure}
    	\end{minipage}
    	
    
    	\item<2->
    	$V = \{a, \dots, h\}$
    	$E = \{\{a, b\}, \dots \{g, e\}\}$,
    	$w : E \rightarrow \mathbb{R}_{+}$
    	
    	\item<3->
    	\begin{minipage}{.45\textwidth}
    		Minimized weight of spanning tree\\
    		$w(T) = \sum_{e \in E} w(e)$
    	\end{minipage}
    	%
	    \begin{minipage}{.45\textwidth}
	    	\begin{figure}
	    		\includegraphics[width=1\textwidth]{figures/simpleGraphMST.png}
	    	\end{figure}
	    \end{minipage}
    	
    \end{itemize}
\end{frame}


\subsection{\boruvkas Algorithm}
\begin{frame}
\frametitle{\boruvkas Algorithm}
	\begin{itemize}
		\item<1-> Greedy algorithm for finding a MST in a graph
		
		\item<2->[-] Starting with each node as an singleton component
		\item<2->[-] successively merging over the \alert{minimum weight outgoing edge (MOE)}
		
		\item<3-> Only adding MOE gives optimal result
	
	\end{itemize}
\end{frame}
\begin{frame}
\begin{itemize}
	\item<1->
	Find MOE for all Components (here all nodes)
	\begin{figure}
		 \includegraphics[width=.45\textwidth]{figures/simpleGraphBoruvka1.png}
	\end{figure}
	
	\item<2-> Merge Components
	\begin{figure}
		 \includegraphics[width=.45\textwidth]{figures/simpleGraphBoruvka2.png}
	\end{figure}
\end{itemize}	
\end{frame}

\begin{frame}
\begin{itemize}
	\item<1->
	Find MOE for both components
	\begin{figure}
		\label{Boruvka3}
		% \caption{Boruvka3}
		\includegraphics[width=.45\textwidth]{figures/simpleGraphBoruvka3.png}
	\end{figure}
	
	\item<2->
	Merge components
	\begin{figure}
		\includegraphics[width=.45\textwidth]{figures/simpleGraphBoruvka4.png}
	\end{figure}
\end{itemize}	
\end{frame}


\section{CONGEST}
\begin{frame}
	\frametitle{CONGEST Model}
	\begin{itemize}
		\item<1-> standard in the theory literature
		\item<2-> algorithm works in synchronous rounds
		\item<3->[-] per round, each node sends one message to each neighbor\\
		(different messages allowed for different neighbors)
		\item<4->[-] usually message size of $\log(n)$
	\end{itemize}
	%\cite{DGA}
\end{frame}

\subsection{MST algorithm in CONGEST}
\begin{frame}
\frametitle{MST algorithm in CONGEST}
\begin{itemize}
	\item<1-> near-optimal algorithm in the CONGEST model following \boruvkas

	\item<2->[-] gradually grow a forest over MOE until a spanning tree is reached

	\item<3-> $O(\log(n))$ phases (at least halving number of fragments)
	
	\item<4-> differentiate components by size ($\sqrt{n}$)
	
	\item<5->[-] small components ($< \sqrt{n}$) communicating over the fragment
	
	\item<6->[-] large components ($> \sqrt{n}$) using the BFS instead
	
	\item<7-> Algorithm needs $O(D + \sqrt{n})$ rounds for one phase\\
	thus solving the problem in \alert{$\tilde{O}\left(D + \sqrt{n}\right)$}
\end{itemize}
%\cite{DGA}
\end{frame}

\section{LogP Model}
\begin{frame}
	\frametitle{LogP Model}
	\begin{itemize}
		\item<1->
		Designed to better capture real networks: 
		
		\item<2->[-]
		$L$ : \alert{Latency} =	 transmission time for a message over an edge
		
		\item<3->[-]
		$o$ : \alert{overhead} = Sending
		and receiving a message takes o time steps each
		(we will allow one processor to receive and send messages at the same time)
		
		\item<4->[-]
		$g$ : \alert{gap} specifies the minimum time required
		between two messages over the same edge
		
		\item<5->[-]
		$p$ : number of \alert{processors} ($p = |V| = n$)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Examples}
	\begin{itemize}
		\item<1-> MSG and answer $= 4 o + 2 L$
		\begin{figure}
			\includegraphics[width=.45\textwidth]{figures/LogP_MSGAnswer.png}
		\end{figure}
%	
%		\item 2 Messages over same edge
%		\begin{figure}
%			\includegraphics[width=.45\textwidth]{figures/LogP_2MSG.png}
%		\end{figure}
%	
		\item<2-> k Messages over same edge $\in O (k \cdot (o + g) + L)$
		\begin{figure}
			\includegraphics[width=.45\textwidth]{figures/LogP_kMSG.png}
		\end{figure}
	\end{itemize}
\end{frame}

\subsection{Simulating a single CONGEST phase}
\begin{frame}
	\frametitle{Simulating a single CONGEST phase}
	\begin{itemize}
		\item<1-> task: send and receive one message to and from each neighbor

		\item<2-> Let $\Delta$ be the degree of $G$

		\item<3->[-] Last message is sent after $\Delta \cdot o$
		\item<4->[-] this message reaches destination after $L$
		\item<5->[-] at most another $\Delta \cdot o$ until message is received 

		\item<6-> possible in \alert{$O(\Delta \cdot o + L)$}  with $(g < L)$
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Simulation Algorithm}
\begin{itemize}
	\item<1-> immediately yields an algorithm to solve the MST problem
	\item<2->[]
	%Naively simulating a CONGEST-\boruvka algorithm with a running time of $\tilde{O}(D + \sqrt{n})$ in the LogP model (with the cost of $O(L + \Delta \cdot o)$ LogP rounds for one round in the CONGEST model), leads to a running time of:
	\begin{align}
	& \tilde{O}\left((D + \sqrt{n}) \cdot (L + \Delta \cdot o) \right)\\
	\label{eq:congest}= \; &\alert{ \tilde{O} \left(D \cdot (L + \Delta \cdot o) + \sqrt{n} \cdot (L + \Delta \cdot o)\right)}
	\end{align}
\end{itemize}
\end{frame}

\section{LogP MST algorithm}
\subsection{Algorithm outline}
\begin{frame}
	\frametitle{Algorithm outline}
	\begin{itemize}
		\item<1-> construct efficient global structure
		\item<2-> aggregate localy (counting the fragment size)
		\item<3-> aggregate globaly once the fragments are big enough
	\end{itemize}
\end{frame}

\subsection{Construction phase}
\begin{frame}
	\frametitle{Construction of a global communication tree}
	\begin{itemize}
		\item<1-> main task : broadcasting and convergecasting
		\item<1-> BFS (as in CONGEST) might be inefficient

		%\item<2-> The best possible communication tree depends heavily on the shape of the G; it is hard to find a tight bound for an arbitrary graph.
		%\item Which of the node is considered to be the root has an impact on the time complexity
		\item<2-> A shortest path tree will be created by a flooding mechanism
		
		\item<3->[-] $\frac{4 \log(n)}{n}$ nodes become active (potential leader)
		
		\item<4->[-] broadcasting join messages, waiting for convergecasted sum of $n$
		
		\item<5->[-] leader with highest ID becomes root 
		
		\item<6->[] \alert{$$O(D \cdot L \cdot \Delta \cdot o)$$}
		\item<7-> Efficiently finding the best scheduling in a distributed setting is a problem on its own, since the scheduling itself is dependent on the chosen edges and the leader position
		\item<8-> The simple tree is sufficiently fast for broadcasting
	\end{itemize}
\end{frame}

\subsection{Local aggregation}

\begin{frame}
\frametitle{Local aggregation algorithm}
\begin{figure}
	\includegraphics[width=.95\textwidth]{figures/localAggregation.png}
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Local aggregation}
	\begin{itemize}
		\item<1->
		finding MOE for individual processor in parallel is possible in $O(L + \Delta \cdot o)$ (one CONGEST round)
		
		\item<2->
		Broadcast and convergecast on fragment $F$ of size $k$; $diam(F) \leq k$\\
		$O (k \cdot (L + \Delta \cdot o))$
		
		\item<3-> cost of one local aggregation \alert{$O(k \cdot (L + \Delta \cdot o))$}
	\end{itemize}
\end{frame}

\subsection{Global aggregation}
\begin{frame}
	\frametitle{Global aggregation}
	\begin{itemize}
		\item<1-> fragments of size $\geq k$ (at most $\lfloor \frac{n}{k} \rfloor$ of them)
		
		\item<2->[-] every individual node finds its MOE
		
		\item<3->[-] leave nodes start a convergecast
		
		\item<4->[-] for every fragment is one convergecast of their MOE $\lfloor \frac{n}{k} \rfloor$
		
		\item<5->[-] results reach root in a pipelined fashion
		
		\item<6-> cost of one global aggregation \alert{$O\left(D \cdot (L + \Delta \cdot o) + \frac{n}{k} \cdot (\Delta \cdot o + g)\right)$}
	\end{itemize}
\end{frame}

\subsection{Balancing local and global aggregation}
\begin{frame}
	\frametitle{Balancing local and global aggregation}
	\begin{itemize}
		\item<1->[]
		\begin{align}
		&k \cdot (L + \Delta \cdot o) = \frac{n}{k} \cdot (\Delta \cdot o + g)\\
		\Leftrightarrow \; &k = \sqrt{n \cdot \frac{\Delta \cdot o + g}{\Delta \cdot o + L}}\\
		\Leftrightarrow \;&k = \sqrt{n} \cdot \sqrt{\frac{\Delta \cdot o + g}{\Delta \cdot o + L}}\\
		\Leftrightarrow \;&\frac{n}{k} =\sqrt{n} \cdot \sqrt{\frac{\Delta \cdot o + L}{\Delta \cdot o + g}}
		\end{align}
		
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Total cost}
	\begin{itemize}
		\item<1-> constructing the global communication tree in $$O(D \cdot L \cdot \Delta \cdot o)$$
		
		\item<2-> $\log(\frac{n}{k})$ local aggregations, each with cost $$O(k \cdot (L + \Delta \cdot o) + g)$$
		
		\item<3-> $\log(k)$ global aggregations, each with cost of $$O\left(D \cdot (L + \Delta \cdot o) + \frac{n}{k} \cdot (\Delta \cdot o + g)\right)$$
		
		\item<4->
		total cost:
		\alert{$$\tilde{O}\left(D \cdot (L + \Delta \cdot o) + \sqrt{n} \cdot \sqrt{\Delta \cdot o + g} \cdot \sqrt{\Delta \cdot o + L}\right)$$}
	\end{itemize}
	
\end{frame}

\section{Result}
\subsection{Improvement}
\begin{frame}
	\frametitle{Improvement}
	\begin{itemize}
		\item<1->
		Simulating the CONGEST algorithm:
		$$\tilde{O} \left(D \cdot (L + \Delta \cdot o) + \sqrt{n} \cdot (L + \Delta \cdot o)\right)$$

		\item<1->
		LogP MST algorithm:
		$$\tilde{O}\left(D \cdot (L + \Delta \cdot o) + \sqrt{n} \cdot \sqrt{\Delta \cdot o + g} \cdot \sqrt{\Delta \cdot o + L}\right)$$
		
		\item<2->
		For $g = L$, the running times are equal
		\item<3->
		For $g < L$, the presented algorithm is faster\\
		
	\end{itemize}
\end{frame}

\subsection{Outlook}
\begin{frame}
	\frametitle{Outlook}
	\section{Outlook}
	\begin{itemize}
		\item
		This work only procured with unit latencies. Further exploration into different latencies models, especially arbitrary latencies, with high correlation between weight and latency of an edge could be interesting.
		
		\item
		By taking another look into the construction of shortest path trees, one could potentially find an algorithm that results in a more efficient way of broadcasting thus improving this work.
	\end{itemize}

\end{frame}

\end{document}
