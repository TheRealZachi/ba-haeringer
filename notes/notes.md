# notes
### Latency, Capacity, and Distributed Minimum Spanning Tree (paper kuhn)

standard CONGEST model  
message size O(log n)
latency

For more realworld:  
___latency___  
Here, we describe the throughput as the ___capacity___ of a link, which we indicate as a fraction in [0, 1]

#### Weighted CONGEST Model.
connected, undirected graph G = (V, E) with n = |V| nodes and m = |E| edges.  
edge is a bidirectional synchronous communication channel that has three attributes associated with it: latency, capacity, and weight

capacity c ∈ (0, 1], it implies
that u can send a new message to v in every 1/c rounds

If an edge (u, v) has latency _l_, it implies that it requires _l_ rounds for a message to be sent from u to v (or vice versa)

assume that _l_ min ≥ 1/c

## PAPER:

* https://www.cs.ubc.ca/~condon/papers/chungcondon96.pdf