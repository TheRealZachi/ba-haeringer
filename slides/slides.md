## Presentation


### Date
* Mail

### Time
* Wie lange der Vortrag?
 * neue PO: 1h Kolloq, davon 20min Vortrag : https://www.tf.uni-freiburg.de/bilder/studium_lehre/po-entwurfsversionen/entwurf-po-b-sc-informatik
 * alte (meine) PO: nichts dazu https://www.jsl.uni-freiburg.de/informationen_fuer_studierende_web/pruefungsordnungen/bachelor_of_science/b_sc__pruefungsordnung_21_12_2015_informatik.pdf/
* Wie lange die Fragen

### Presentation
* Worauf soll ich den Fokus legen?
  * Also was ist das Ziel des Vortrages

relativ Zielstrebig auf das Hauptproblem / Hauptergebnis ausrichten:

* Problem formulieren (MST)
* Model erklären (logP)
  * dafür zuerst CONGEST erklären, 
* Boruvka in CONGEST erklären
  * einzeln in LogP übersetzen
    * bfs baum, lokale phase, globale phase
* Laufzeit naiv simuliert im Vergleich mit nutzen von pipelineing
 * Laufzeitgewinn

### Kolloq
* Auf was für Fragen soll ich mich vorbereiten?
