\section{Balancing Local and Global Aggregation}

One aggregation step in the local phase can be computed in $$O(k \cdot (L + \Delta \cdot o))$$ processor cycles,
while one phase in the global aggregation phase can be computed in $$O\left(D \cdot (L + \Delta \cdot o) + \frac{n}{k} \cdot (\Delta \cdot o + g)\right)$$

\subsection{Finding the ideal Fragment Size}
By using the edge cases $1 \leq k \leq n$ of the possible fragment size, we gain insight on the complexity:

$k = 1:$
\begin{equation}
	O\left((L + \Delta \cdot o)\right) < O(D \cdot (L + \Delta \cdot o) + n \cdot (\Delta \cdot o + g))
\end{equation}

For small fragments the local phase is favourable. 
The dependency on the term $L + \Delta \cdot o$ is present on both sides, but the global phase has an additional factor of $D$ for it.

$k = n:$
\begin{equation}
	O(n \cdot (L + \Delta \cdot o)) > O(D \cdot (L + \Delta \cdot o) +  (\Delta \cdot o + g))
\end{equation}

For large fragments the global phase is favourable:
the diameter as a factor is never worse than the number of nodes, depending on the graph $D$ can get as small as a constant factor.

In the CONGEST model the usual fragment size to swap phases  is $\sqrt{n}$ \cite{lecture}. The optimal fragment size to minimize the sum of the costs can be found by equating the cost per round and solving for $k$.
The cost of the global aggregation is the sum of two terms, but only one of them has a dependency on $k$.

\begin{equation}
	k \cdot (L + \Delta \cdot o) = \frac{n}{k} \cdot (\Delta \cdot o + g)
\end{equation}

\begin{align}
	&k \cdot (L + \Delta \cdot o) = \frac{n}{k} \cdot (\Delta \cdot o + g)\\
	\Leftrightarrow \; &k = \sqrt{n \cdot \frac{\Delta \cdot o + g}{\Delta \cdot o + L}}\\
	\Leftrightarrow \;&k = \sqrt{n} \cdot \sqrt{\frac{\Delta \cdot o + g}{\Delta \cdot o + L}}\\
	\Leftrightarrow \;&\frac{n}{k} =\sqrt{n} \cdot \sqrt{\frac{\Delta \cdot o + L}{\Delta \cdot o + g}}
\end{align}

The ideal fragment size to initiate the global aggregation phase is $k = \sqrt{n} \cdot \sqrt{\frac{\Delta \cdot o + g}{\Delta \cdot o + L}}$

%\subsubsection{more detailled calculation}
%
%\begin{equation}
%	O(k \cdot (L + \Delta \cdot o)) = O\left(D \cdot (L + \Delta \cdot o) + \frac{n}{k} \cdot (\Delta \cdot o + g)\right)
%\end{equation}
%
%\begin{align}
%	0 &= k^{2} \cdot (L + \Delta \cdot o) - k \cdot D \cdot (L + \Delta \cdot o) - n \cdot (\Delta \cdot o + g)\\
%	k_{1/2} &= \frac{D \cdot (L + \Delta \cdot o) \pm \sqrt{\left(D \cdot (L + \Delta \cdot o)\right)^{2} + 4 n \cdot (L + \Delta \cdot o) \cdot (\Delta \cdot o + g)}}{2 \cdot (L + \Delta \cdot o)}\\
%	k_{1/2} &= \frac{D}{2} \pm  \sqrt{\frac{D^{2}}{4} + \frac{n \cdot (\Delta \cdot o + g)}{L + \Delta \cdot o}}
%\end{align}
%
%we are not interested in a negative solution for $k$.
%
%$$k = \frac{D}{2} + \sqrt{\frac{D^{2}}{4} + \frac{n \cdot (\Delta \cdot o + g)}{L + \Delta \cdot o}}$$
%
%\begin{itemize}
%	\item with that assumption that $g \in \Theta(L)$ it can be simplified to $k = \frac{D}{2} + \sqrt{(\frac{D^2}{4} + n)}$
%	\item with the assumption that $g \in \Theta(o)$ it can be simplified to $k = \frac{D}{2} + \sqrt{(\frac{D^2}{4} + \frac{n}{L})}$
%	
%\end{itemize}
%
%\section{Cost Analysis}
%
%The cost are:
%\begin{itemize}
%	\item constructing the global communication tree in
%	\item $\log(\frac{n}{k})$ phases in local
%	\item each with cost $O(k \cdot (L + \Delta \cdot o) + g)$
%	\item $\log(k)$ phases in global
%	\item each with cost of $O\left(D \cdot (L + \Delta \cdot o) + \frac{n}{k} \cdot (\Delta \cdot o + g)\right)$
%\end{itemize}
%
%\todo{add construction of tree to cost}
%$$ \log \left(\frac{n}{k} \right) \cdot O(k \cdot (L + \Delta \cdot o) + g) + \log(k) \cdot O\left(D \cdot (L + \Delta \cdot o) + \frac{n}{k} \cdot (\Delta \cdot o + g)\right)$$

\section{Cost Analysis}
The total time until the cost consist of:
\begin{itemize}
	\item constructing the global communication tree in $O(D \cdot L \cdot \Delta \cdot o)$
	\item $\log(\frac{n}{k})$ local aggregations, each with cost $O(k \cdot (L + \Delta \cdot o) + g)$
	\item $\log(k)$ global aggregations, each with cost of $O\left(D \cdot (L + \Delta \cdot o) + \frac{n}{k} \cdot (\Delta \cdot o + g)\right)$
\end{itemize}

The following three equations are given in $\tilde{O}(\cdot)$ notation, omitting the $\log(n)$ factors from the number of aggregations necessary.
\begin{align}
	&k \cdot (L + \Delta \cdot o) + D \cdot (L + \Delta \cdot o) + \frac{n}{k} \cdot (\Delta \cdot o + g)\\
	= \; &\sqrt{n} \cdot \sqrt{\frac{\Delta \cdot o + g}{\Delta \cdot o + L}} \cdot (L + \Delta \cdot o) + D \cdot (L + \Delta \cdot o) + \sqrt{n} \cdot \sqrt{\frac{\Delta \cdot o + L}{\Delta \cdot o + g}} \cdot (\Delta \cdot o + g)\\
	\label{eq:total}= \; &D \cdot (L + \Delta \cdot o) + \sqrt{n} \cdot 2 \cdot \sqrt{\Delta \cdot o + g} \cdot \sqrt{\Delta \cdot o + L}
\end{align}

The provided algorithm solves the MST problem in $$\tilde{O}\left(D \cdot (L + \Delta \cdot o) + \sqrt{n} \cdot \sqrt{\Delta \cdot o + g} \cdot \sqrt{\Delta \cdot o + L}\right)$$