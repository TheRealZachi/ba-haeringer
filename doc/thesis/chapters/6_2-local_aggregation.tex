\section{Local Aggregation}
%Die lokale Aggregation. Angenommen k ist die maximale Größe des Fragments. Jeder Knoten v des Fragments bestimmt seine MOE (minimum outgoing edge), also die Kante mit minimalem Gewicht, welche in ein anderes Fragment geht. Dazu muss über jede Kante je eine Nachricht in jede Richtung gesendet werden. Dies dauert Delta*o+L. Danach werden die MOE der Knoten über den BFS-Baum des Fragments aggregiert. Dazu sendet, angefangen bei den Blättern, jeder Knoten seine MOE zu seinem parent. Bis ein Knoten die Nachrichten aller seiner Kinder erhalten hat, dauert es Delta*o+L. Nun wählt ein Knoten die kleinste Kante aus, die er erhalten hat und leitet sie an seinen parent weiter. Wenn das Fragment maximal k Knoten hat, so ist auch der Durchmesser maximal k und es dauert daher k*(Delta*o+L), bis der leader die MOE des Fragments gelernt hat. 
\subsection{Bottom up Approach}

The local aggregation phase will follow a distributed version of \boruvkas algorithm, where a forest of n singleton fragments successively merge over their current minimum weight outgoing edges.
Thus gradually connecting trees and decreasing the fragment amount in the forest.
This approach could be pursued until the last remaining fragment is the resulting minimum spanning tree. Communicating on large fragments is a slow process, thus the fragments are only allowed to aggregate to a fragment size of $k$, before using the global aggregation.


\subsection{Determining MOE of a Single Processor}
\label{sec:MOESingleNode}

\begin{lemma}[finding the MOE of a fragment]
	Let $F$ be a fragment in the local aggregation phase.
	Finding its minimum outgoing edge is possible in $O(L + \Delta \cdot o)$ cycles.
\end{lemma}

To determine the minimum outgoing edge of a fragment, it is necessary to find those of the individual nodes first and comparing them subsequently.
Finding the MOE of a single processor is composed of two properties: finding the (i) minimum edge of the node and guaranteeing that this edge is (ii) outgoing.
\begin{itemize}
	\item[(i)] Finding the minimum of its neighbors is possible in $O(\Delta)$ steps, by simply internally comparing the values in the list of incident edges. To simplify the problem the assumption that each processor has a sorted list of its edge weights to begin with is made. Such a list can be created in $O(\Delta \cdot \log(\Delta))$ by simply sorting the list once in the beginning.
	
	\item[(ii)] Whether or not a node $v$ is connected with another node $u$ does not only depend on the existence and status of the edge $\{v, u\}$, $v$ and $u$ might be connected indirectly, for example over a third node $w$, if the edges $\{v, w\}$ and $\{w, u\}$ were MOE of previous rounds.
\end{itemize}

There are several different approaches to solve this problem:

\begin{itemize}
	\item If it is possible for each node to always know if adjacent nodes are in the same fragment, the additional communication cost can be completely saved, this might be a possible solution for a single fragment growing and absorbing one new node at a time and learning the identity of all new nodes in an alternated version of the already necessary messages, however merging two or more fragments of size $k$ results in $O(k)$ messages that need to be shared over the whole new fragment.
	
	\item Exchanging information while the nodes are idle, to reduce message exchanges in busy times could potentially save some time as well. A node might send a message to its neighbors whenever it got the information that it merged, if a processor learns that it shares the same fragment with his neighbor, it will never have to send a message over the edge connecting them, since it can never be an outgoing edge again. However this idea is not further explored here.
	
	\item The most promising idea is to communicate over the MOE-candidates, exchanging the current fragment ID. While a single node can ask and get a result from the cheapest edge in $4o + 2L$ (as seen in \nameref{lem:reading_remote_location})  this can get more expensive if a node has to respond to a potential of $\Delta$ neighbors at the same time. The even less desirable outcome would be to discover after waiting for the response that the minimum edge already is in the same component. A waiting time $\in O(\Delta \cdot (L + o)$ until a processor confirms to be in a suitable fragment is not practicable to reduce the time complexity, and should only be considered if the message complexity is more important. Instead a node will ask all adjacent nodes, ascending by their weight until a node with a distinct fragment ID has responded. It is not necessary to further send messages after an outgoing edge is found, since the remaining edges are all of higher weight. The time complexity is modeled in the section \nameref{sec:all neighbors} and is possible in $O(L + (\Delta \cdot o) + g)$.
	For this special case, the parameter $g$ can be ignored by adapting the message: Instead of asking for the adjacent fragment id and sending the response in an additional message, the information will be packed in the same message. It is possible that a node will receive an respond it did not request that way, but there is no additional cost and the unwanted information can simply be ignored.
\end{itemize}

\subsection{Control Merges with Coin Tossing}
Tossing a coin symbolizes randomly choosing one of two equally likely values.
Sticking with the coin idea, the result will be $\in \{\text{head}, \text{tail}\}$.
The advantage of using a randomized property for fragments is twofold, it prevents cyclic parallel merges and helps with keeping the fragment diameter small.
The terms \textit{head-fragment} and \textit{tail-fragment} will be used to describe a fragment with a leader that has the respective result in the current round.

\begin{itemize}
	\item The following advantage only comes into effect if the weight of the edges are not distinct and breaking the ties is not possible.
	Let $A, B, C \subset G$ be subtrees of a Graph $G = (V, E)$, with a pairwise disjoint set of nodes $V_{A}, V_{B}, V_{C} \subset V$.
	Further let $a, b, c$ be the minimum outgoing edges of $A, B, C$ respectively, such that $a$ connects $A$ with $B$, $b$ connects $B$ with $C$ and $c$ connects $C$ with $A$\footnote{this scenario is not possible for distinct edge weights, since $ w(a) \geq w(b)$ and $w(b) \geq w(c)$ and $w(c) \geq w(a)$ implies $w(a)=w(b)=w(c)$}.
	The attempt to merge the trees $A, B, C$ by merging over the three edges $a, b$ and $c$ in parallel would result in adding a cycle. While merging over a single MOE in a non-distributed setting at a time results in a correct MST, one cannot take multiple edges at a time and expect a correct result without taking precautionary measures.
	To prevent the just mentioned problem, only head-fragments will be allowed to accept merges. Tail-fragments will ask to merge over their MOE, this will only be allowed if the adjacent fragment is a head-fragment.
	Merging in this fashion makes it impossible for a cycle to appear. There will never be more than one head-fragment in a merging set of fragments, since a head-fragment cannot propose merges themself and the tail-fragments all chose the same fragment.
	The other possibility to close a circle at two tail-fragments that are both connected with the same head-fragment. This would require that an edge between two tail-fragments was chosen, which is not allowed. We end up with a head fragment in the middle and arbitrary many tail-fragments in a star-shape around it.
	
	\item As already described in the previous point, the merges happen in a star-shape around the head-fragment.
	Restricting the form of merges limits the diameter growth of the resulting fragment.
	Let $k$ be the maximal diameter of any fragment in $G$.
	After one phase of accepting star-shaped merges, the maximal diameter for a fragment is $3 \cdot k + 2$, since two merge edges can theoretically connect three paths of length $k$.
	The fragment diameter is upper bound by $3^t + 2t$, where $t$ is the amount of phases that have passed.
\end{itemize}


\subsection{Description of the Local Phase}
At the beginning of each round, the fragment leader will toss a coin that will
determine the behaviour of a fragment for this round.
The result will be broadcast over, if it is \textit{tail}, the leader will additionally ask its fragment to identify the fragments MOE.
The message is spread over the edges of the fragment, that have been previously used to merge. Each non-leave node will propagate the message to its children before finding its own MOE. The leave nodes will immediately start finding their MOE.
To find the MOE edge, every node will work through its weight ascending ordered list of incident edges. If a node happens to know that a neighbor already is in the same fragment (as a result of a previous round) the corresponding edge can be skipped, since it can no longer be part of the spanning tree.
As previously discussed, waiting for a response before contacting the next node would result in a better message complexity but also increase the time until a node that is not in the same fragment is found. For this reason any node searching for his MOE will continue sending messages to his adjacent nodes until the \textit{correct} edge is found or the list is exhausted. An edge is considered to be \textit{correct} if the edge is the first in the ascending list that is not in the same fragment. 
Now that all nodes know their personal best MOE-candidate, this information is getting collected by a minimum convergecast over the same structure.
As soon as a leave node found its MOE or knows that none of its edges are outgoing anymore, it will send this information to its parent.
A non-leave and non-root node that received the information of all its children, and has found its own MOE, will compare the weights of all MOE candidates and forward the minimum weight edge or the information that there is no outgoing edge left in its sub-tree.
With the receiving of the last message at the root, the MOE of the complete fragment will be known.
The leader broadcasts the information about the MOE, including the current fragment size; the node incident to the MOE is requested to ask for a merge.
If the second incident node is in a head-fragment the merge is accepted and the head-fragment absorbs the tail-fragment.
The structure of the head-fragment does not need many adjustments, only the nodes accepting merges will need to add the accepted edge as a children.
The tail-fragment needs to make the head-fragment leader its new leader, by changing the fragment id and the direction of edges. The nodes already know whether or not they need to change their hierarchy, since they can compare the MOE of the fragment with the MOE-candidate they sent to their parent. The nodes that need to change their children and parent pointers are exactly the nodes that have the successful MOE-candidate in its sub-tree and thus know the origin of the information.
The head-fragment can sum up the added nodes in a convergecast for the fragment leader to update the component size.

\begin{lemma}[Fragment number reduction]
	\label{lem:local_phases_requirerd}
	After $\log(k)$ local aggregation rounds the number of fragments is reduced to $\Theta ( \frac{n}{k} )$ with high probability.
	For $k=n$ in particular: after $\log(n)$ rounds the number of fragments is reduced to $O(1)$ with high probability.
\end{lemma}

Each fragment suggest one merge edge. Each of these edges could potentially be suggested by two fragments. After removing the duplicates, there are a minimum of $\lceil \frac{n}{2} \rceil$ distinct edges suggested each round.
The amount of fragments is at least getting halved each round.
The coin augmentation only increases the amount of required rounds by a constant factor w.h.p. \cite{lecture}.

\begin{lemma}[Diameter of fragments]
	The number of nodes in a fragment is an upper bound for the diameter of the fragment.
\end{lemma}

\begin{algorithm}
	\caption{one phase in the local aggregation}
	\begin{algorithmic}[1]
		\State leader tosses a coin $\{\text{head}, \text{tail}\}$; the result will determine the behaviour of the whole fragment for this phase. Head-fragments will be the middle point of incoming merges of tail-fragments.
		
		\State Tail-fragments will find their MOE by broadcasting, exchanging fragment-IDs with adjacent nodes and convergecasting the minimum back to the leader.
		
		\State Let $e = \{t, h\}$ be the MOE of the fragment of $t$. The leader of the tail fragment send the message to $t$, confirming that his MOE is the best of the whole fragment together with the current fragment size. The fragments will merge iff $h$ confirms that it is in a head-fragment.
		
		\State All nodes in the head fragment will add the nodes of successful merges as children and convergecast the amount of new nodes in the fragment to the leader.
		
		\State The tail-fragment adapts is children and parent pointer, defining the new hierarchy. This can be done while the head fragment is counting the new nodes.
	\end{algorithmic}
\end{algorithm}

\section{Cost of one Local Aggregation Phase}
\label{sec:cost_local}
The cost of a single local aggregation phase accumulates as follows:


\begin{itemize}
	\item Broadcast and convergecast on a fragment $F$ of a maximal size of $k$ is possible in $O( k \cdot (\Delta \cdot o + L))$ since $diam(F) \leq k$.
		
	\item finding the MOE for each processor in parallel is possible in $O(L + \Delta \cdot o)$
	
	\item arranging the merges does not take more time than finding the MOE
\end{itemize}

\begin{lemma}[Local Aggregation Cost]
	\label{lem:local_cost}
	Let $k$ be the size of the largest fragment at the beginning of the current phase. A single round of the local aggregation phase can be computed in $O(k \cdot (L + \Delta \cdot o))$. 
\end{lemma}

\begin{lemma}[MST in local phase]
	Simply using \ref{lem:local_cost} and \ref{lem:local_phases_requirerd} results in an algorithm that can solve the MST problem in a time complexity of
	$O(\log(n) \cdot n \cdot (\cdot (L + \Delta \cdot o)))$.
\end{lemma}